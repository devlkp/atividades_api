<?php

namespace App\Http\Controllers\Api;

use App\Activities;
use App\Atividades;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AtividadesController extends Controller
{
    private $activitie;

    public function __construct(Activities $act)
    {
        $this->activitie = $act;
    }

    public function index(){
        return Activities::all();
    }

    public function getId(Activities $id){
        return $id;
    }

    public function post(Request $req){
        $activitieData = $req->all();
        $this->activitie->create($activitieData);
    }

    public function delete(Activities $id){
        try {
            $id->delete();
            return response()->json(['data' => ['msg' => "Activitie ".$id->id." Foi removido com sucesso!"]], 200);
        } catch (\Exception $e) {               
            return response()->json(['data' => ['msg' => 'Houve um erro ao realizar operacao de remover']],500);
        }
    }

    public function update(Request $req, $id){
        $activitieData = $req->all();
        $activitie = $this->activitie->find($id);
        $activitie->update($activitieData);
        return response()->json(['data' => ['msg' => 'Atividade atualizada com sucesso!']], 200);
    }
   
}
